import React from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { connect } from "react-redux";

const Counter = ({ value, handleClick }) => {
  return (
    <div className="container-fluid my-3">
      <Button size="sm" onClick={() => handleClick("INCREMENT_ASYNC")}>
        Increment after 1 second
      </Button>{" "}
      <Button size="sm" onClick={() => handleClick("INCREMENT")}>
        Increment
      </Button>{" "}
      <Button size="sm" onClick={() => handleClick("DECREMENT")}>
        Decrement
      </Button>
      <hr />
      <div>Clicked: {value} times</div>
    </div>
  );
};

Counter.propTypes = {
  value: PropTypes.number,
  handleClick: PropTypes.func
};

export default connect(
  state => {
    return { value: state.counter };
  },
  dispatch => {
    return {
      handleClick: type => {
        dispatch({ type: type });
      }
    };
  }
)(Counter);
