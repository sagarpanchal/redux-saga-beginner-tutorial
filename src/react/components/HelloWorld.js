import React, { Component } from "react";
import PropTypes from "prop-types";
import { Col, Jumbotron, Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import { sayHello } from "../actions";

class HelloWorld extends Component {
  static propTypes = {
    text: PropTypes.oneOfType([PropTypes.string]),
    handleClick: PropTypes.func
  };

  render() {
    const { text, handleClick } = this.props;
    return (
      <div className="d-flex flex-row justify-content-center m-3">
        <Col sm="12" md="8" className="p-0">
          <Jumbotron>
            {text ? (
              <h2>
                {text} <FontAwesomeIcon icon="hand-peace" />
              </h2>
            ) : null}
            <p className="mb-0">
              MixReact is a simple react template with useful packages.
              <sup>
                <small> See package.json</small>
              </sup>
            </p>
            <small>Made with laravel-mix</small>
          </Jumbotron>
          <h5>
            NPM scripts<sup>
              <small> See README.md</small>
            </sup>
          </h5>
          <p>
            <strong className="text-light bg-dark font-weight-light">
              &nbsp;npm run watch&nbsp;
            </strong>
            &nbsp;for live updates while testing,<br />
            <strong className="text-light bg-dark font-weight-light">
              &nbsp;npm run dev&nbsp;
            </strong>
            &nbsp;for development build and<br />
            <strong className="text-light bg-dark font-weight-light">
              &nbsp;npm run prod&nbsp;
            </strong>
            &nbsp;for production build.
          </p>
          <a className="lead">
            <Button color="primary" className="mb-2" onClick={handleClick}>
              Hello
            </Button>
          </a>
        </Col>
      </div>
    );
  }
}

export default connect(
  state => {
    return { text: state.reducer.text };
  },
  dispatch => {
    return {
      handleClick: () => {
        dispatch(sayHello("Hello, World!"));
      }
    };
  }
)(HelloWorld);
