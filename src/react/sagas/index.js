import { all, delay, put, takeEvery } from "redux-saga/effects";

function* incrementAsync(ms) {
  yield delay(ms);
  yield put({ type: "INCREMENT" });
}

export function* watchIncrementAsync() {
  yield takeEvery("INCREMENT_ASYNC", incrementAsync, 1000);
}

export default function* rootSaga() {
  yield all([watchIncrementAsync()]);
}
