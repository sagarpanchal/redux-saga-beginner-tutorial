import React, { Component } from "react";
import "./libraries/FontAwesome";
import Counter from "./components/Counter";

export default class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Counter />
      </React.Fragment>
    );
  }
}
