import reducer from "./reducer";
import counter from "./counter";

export default { reducer, counter };
